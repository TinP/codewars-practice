/*
"din"      =>  "((("
"recede"   =>  "()()()"
"Success"  =>  ")())())"
"(( @"     =>  "))(("
*/

function testCases(inputString) {
    var temp = inputString.toLowerCase().split("");
    var emptyString = "";

    for(let i=0;i<temp.length;i++){
        const filteredArr = temp.filter(el =>{
            return el === temp[i];
        })
        if (filteredArr.length>1){
            emptyString +=")";
        }
        else {
            emptyString +="("
        }
    }
    return emptyString
}

console.log(testCases("din"));
console.log(testCases("recede"));
console.log(testCases("Success"));
console.log(testCases("Success"));
console.log(testCases("(( @"));