function digitalRoot(int){
    const numsArr = int.toString().split("");
    const total = numsArr.reduce(
        (a,b) => parseInt(a) + parseInt(b));
    if(total > 9){
        return digitalRoot(total);
    }
    return total;
}

console.log(digitalRoot(12345));