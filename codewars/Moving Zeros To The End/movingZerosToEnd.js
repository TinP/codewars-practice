// moveZeros([false,1,0,1,2,0,1,3,"a"]) // returns[false,1,1,2,1,3,"a",0,0]


function moveZeros(arr) {
    arr.sort((a, b) => (a === 0) - (b === 0));
    console.log(arr);
}


moveZeros([false,1,0,1,2,0,1,3,"a"]);
