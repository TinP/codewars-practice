function towerBuilder(nFloors) {
    let main = [];
    if(nFloors===0 || isNaN(nFloors)){
        return 0;
    }
    for(let i=0;i<nFloors;i++){
        let space = " ".repeat(nFloors - i - 1);
        let star = "*".repeat(2*i+1);
        main.push(`${space}${star}${space}`);
    }
    return main;
}


console.log(towerBuilder(5))


/*
For example, a tower with 3 floors looks like this:
[
    "  *  ",
    " *** ",
    "*****"
]


And a tower with 6 floors looks like this:
[
    "     *     ",
    "    ***    ",
    "   *****   ",
    "  *******  ",
    " ********* ",
    "***********"
]
*/