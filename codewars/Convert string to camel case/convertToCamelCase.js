// "the-stealth-warrior" gets converted to "theStealthWarrior"
// "The_Stealth_Warrior" gets converted to "TheStealthWarrior"


function toCamelCase(str){
    let tempStr = str.replace(/[\W_]+/g, ' ').split(" ");

    // console.log(tempStr[0][0]);
    if(tempStr[0][0]===tempStr[0][0].toUpperCase()){
        mainStr = tempStr.map(el =>{
            return el.charAt(0).toUpperCase() + el.slice(1).toLowerCase();
        }).join("");
    }
    else{
        mainStr = tempStr.map(el =>{
            if(tempStr.indexOf(el)===0){
                return el.charAt(0).toLowerCase() + el.slice(1).toLowerCase();
            }
            return el.charAt(0).toUpperCase() + el.slice(1).toLowerCase();
        }).join("");
    }
    return mainStr;
}


console.log(toCamelCase("this-is-a-sentence"));
