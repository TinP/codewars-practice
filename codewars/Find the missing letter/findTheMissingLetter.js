/*
["a","b","c","d","f"] -> "e"
["O","Q","R","S"] -> "P"

*/


function sortString(inputString){
    const a = "abcdefghijklmnopqrstuvwxyz";

    if (inputString[0] == inputString[0].toUpperCase()) {
        let first = a.toUpperCase().indexOf(inputString[0]);
        console.log(findMissingLetter(a.toUpperCase(), first, inputString));
    }
    else {
        let first = a.indexOf(inputString[0]);
        console.log(findMissingLetter(a, first, inputString));
    }

}

function findMissingLetter(str, first, inputString){
    let firstLetter = str.indexOf(inputString[0])

    for(let i = 0; i < inputString.length;i++){
        if(str[firstLetter+i] != inputString[i]){
            return str[firstLetter+i];
        }
    }
}



const a = ["a","b","c","d","f"];
const b = ["O","Q","R","S"];

sortString(a);

sortString(b);
