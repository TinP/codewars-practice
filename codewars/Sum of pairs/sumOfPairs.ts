function sumPairs(nums: number[], sum: number): [number, number] | void {
    let result = 0;
    for(let i = 0; i<nums.length;i++){
        for(let j = i+1;j<nums.length;i++){
            if(nums[j]+nums[i]==sum){
                return [nums[j], nums[i]]
            }
        }
    }
}



console.log(sumPairs([4,3,2,3,4], 6));
// console.log(sumPairs([11,3,7,5], 10));
// console.log(sumPairs([0,0,-2,3], 3));